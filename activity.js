// ITEM 3

db.rooms.insertOne(
    {
        name: "single",
        accomodation: 2,
        price: 1000,
        description: "A simple room with all the basic necessities.",
        rooms_available: 10,
        isAvailable: false
     }
)


// ITEM 4

db.rooms.insertMany([
    {
        name: "double",
        accomodation: 3,
        price: 2000,
        description: "A room fit for a small family going on a vacation.",
        rooms_available: 5,
        isAvailable: false
     },
     {  
        name: "queen",
        accomodation: 4,
        price: 4000,
        description: "A room with a queen sized bed perfect for a simple getaway.",
        rooms_available: 15,
        isAvailable: false
     }
     
])


// ITEM 5
db.rooms.find(
    {name: "double"}
)


// ITEM 6
db.rooms.updateOne(
    {name: "queen"},
    {
        $set: {
            rooms_available: 0
            }
    }
)


// ITEM 7
db.rooms.deleteMany(
    {rooms_available: 0}
)


